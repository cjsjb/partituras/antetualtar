\context Staff = "soprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Soprano"
	\set Staff.shortInstrumentName = "S."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-soprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble"
		\key e \major

		R1*2  |
		r4 gis' e' 8 b 4 cis' 8 ~  |
		cis' 1  |
%% 5
		r4 b cis' 8 dis' 4 e' 8 ~  |
		e' 2 e'  |
		r4 gis' e' 8 b 4 cis' 8 ~  |
		cis' 2 cis'  |
		r4 b cis' 8 dis' 4 e' 8 ~  |
%% 10
		e' 2 e'  |
		r4 gis' e' 8 b 4 cis' 8 ~  |
		cis' 1  |
		r4 b cis' 8 dis' 4 e' 8 ~  |
		e' 2 e'  |
%% 15
		r4 gis' e' 8 b 4 cis' 8 ~  |
		cis' 2 cis'  |
		r4 b cis' 8 dis' 4 e' 8 ~  |
		e' 2 e' ~  |
		e' 2. r4  |
%% 20
		R1*4  |
		fis' 8 fis' fis' fis' 4 fis' 8 fis' fis' ~  |
%% 25
		fis' 8 fis' 4 fis' fis' fis' 8  |
		e' 8 e' fis' gis' ~ gis' 2 ~  |
		gis' 2. r4  |
		R1*4  |
		fis' 8 fis' fis' fis' 4 fis' 8 fis' fis' ~  |
		fis' 8 fis' 4 fis' fis' fis' 8  |
		fis' 4. e' dis' 4  |
%% 35
		e' 1  |
		r4 gis' e' 8 b 4 cis' 8 ~  |
		cis' 2. r4  |
		a' 4. gis' fis' 4  |
		e' 1  |
%% 40
		R1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-soprano" {
		An -- te "tu al" -- tar, __
		hoy co -- "mo her" -- ma -- nos,
		el pan "y el" vi -- no
		te pre -- sen -- ta -- mos.

		An -- te "tu al" -- tar, __
		hoy co -- "mo her" -- ma -- nos,
		el pan "y el" vi -- no
		te pre -- sen -- ta -- mos.

		%Fru -- to del su -- dor
		%y del es -- fuer -- zo del hom -- bre, __
		"...es" trans -- for -- ma -- "do en" el
		cuer -- "po y" san -- gre
		de nues -- tro Se -- ñor. __

		%Fru -- to del su -- dor
		%y del es -- fuer -- zo del hom -- bre, __
		"...es" trans -- for -- ma -- "do en" el
		cuer -- "po y" san -- gre
		de nues -- tro Se -- ñor.

		An -- te "tu al" -- tar, __
		an -- te "tu al" -- tar.
	}
>>
