\context Staff = "tenor" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Tenor"
	\set Staff.shortInstrumentName = "T."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-tenor" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble_8"
		\key e \major

		R1*2  |
		r4 gis e 8 b, 4 cis 8 ~  |
		cis 1  |
%% 5
		r4 b, cis 8 dis 4 e 8 ~  |
		e 2 e  |
		r4 gis e 8 b, 4 cis 8 ~  |
		cis 2 cis  |
		r4 b, cis 8 dis 4 e 8 ~  |
%% 10
		e 2 e  |
		R1  |
		r4 a a 8 gis 4 fis 8 ~  |
		fis 1  |
		r4 gis gis 8 a 4 b 8 ~  |
%% 15
		b 2 b (  |
		a 1  |
		fis 1  |
		gis 1 ~  |
		gis 2. ) r4  |
%% 20
		a 8 a a a a 2  |
		r8 a a a a b cis' b ~  |
		b 4. dis 8 ( ~ dis 2  |
		e 2. ) r4  |
		a 8 a a a 4 a 8 a a ~  |
%% 25
		a 8 a 4 a a a 8  |
		gis 8 gis a b ~ b 2 ~  |
		b 2. r4  |
		a 8 a a a a 2  |
		r8 a a a a b cis' b ~  |
%% 30
		b 4. dis 8 ( ~ dis 2  |
		e 2. ) r4  |
		a 8 a a a 4 a 8 a a ~  |
		a 8 a 4 a a a 8  |
		a 4. gis fis 4  |
%% 35
		gis 1  |
		r4 gis e 8 b, 4 cis 8 ~  |
		cis 2. r4  |
		cis' 4. b a 4  |
		b 1  |
%% 40
		R1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-tenor" {
		An -- te "tu al" -- tar, __
		hoy co -- "mo her" -- ma -- nos,
		el pan "y el" vi -- no
		te pre -- sen -- ta -- mos.

		An -- te "tu al" -- tar, __
		hoy co -- "mo her" -- ma -- nos...
		%el pan "y el" vi -- no
		%te pre -- sen -- ta -- mos.

		Fru -- to del su -- dor
		y del es -- fuer -- zo del hom -- bre, __
		es trans -- for -- ma -- "do en" el
		cuer -- "po y" san -- gre
		de nues -- tro Se -- ñor. __

		Fru -- to del su -- dor
		y del es -- fuer -- zo del hom -- bre, __
		es trans -- for -- ma -- "do en" el
		cuer -- "po y" san -- gre
		de nues -- tro Se -- ñor.

		An -- te "tu al" -- tar, __
		an -- te "tu al" -- tar.
	}
>>
