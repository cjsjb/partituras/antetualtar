\context Staff = "mezzosoprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Mezzosoprano"
	\set Staff.shortInstrumentName = "M."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-mezzosoprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble"
		\key e \major

		R1*2  |
		r4 gis' e' 8 b 4 a 8 ~  |
		a 1  |
%% 5
		r4 a a 8 a 4 gis 8 ~  |
		gis 2 gis  |
		r4 gis gis 8 gis 4 a 8 ~  |
		a 2 a  |
		r4 a a 8 a 4 gis 8 ~  |
%% 10
		gis 2 gis  |
		r4 gis gis 8 gis 4 a 8 ~  |
		a 1  |
		r4 a a 8 a 4 gis 8 ~  |
		gis 2 gis  |
%% 15
		r4 gis gis 8 gis 4 a 8 ~  |
		a 2 a  |
		r4 a a 8 a 4 gis 8 ~  |
		gis 2 gis ~  |
		gis 2. r4  |
%% 20
		R1*4  |
		cis' 8 cis' cis' cis' 4 cis' 8 cis' b ~  |
%% 25
		b 8 b 4 b b b 8  |
		b 8 b b b ~ b 2 ~  |
		b 2. r4  |
		R1*4  |
		a 8 a a a 4 a 8 a b ~  |
		b 8 b 4 b b b 8  |
		cis' 4. b a 4  |
%% 35
		gis 1  |
		r4 gis gis 8 gis 4 a 8 ~  |
		a 2. r4  |
		cis' 4. b a 4  |
		gis 1  |
%% 40
		R1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-mezzosoprano" {
		An -- te "tu al" -- tar, __
		hoy co -- "mo her" -- ma -- nos,
		el pan "y el" vi -- no
		te pre -- sen -- ta -- mos.

		An -- te "tu al" -- tar, __
		hoy co -- "mo her" -- ma -- nos,
		el pan "y el" vi -- no
		te pre -- sen -- ta -- mos.

		%Fru -- to del su -- dor
		%y del es -- fuer -- zo del hom -- bre, __
		"...es" trans -- for -- ma -- "do en" el
		cuer -- "po y" san -- gre
		de nues -- tro Se -- ñor. __

		%Fru -- to del su -- dor
		%y del es -- fuer -- zo del hom -- bre, __
		"...es" trans -- for -- ma -- "do en" el
		cuer -- "po y" san -- gre
		de nues -- tro Se -- ñor.

		An -- te "tu al" -- tar, __
		an -- te "tu al" -- tar.
	}
>>
